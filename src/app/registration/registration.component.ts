import { Component } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Location } from '@angular/common';
import { md5 } from '../md5/md5.component';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: "registration",
	templateUrl: "registration.component.html",
	styleUrls: ["registration.component.css"]
})

export class RegistrationComponent {
	is_auth=localStorage.getItem("user_key")!=null;
	
	login: string; //login user
	password: string; //password user
	password2: string; //repeat password user
	message_user: string; //message for user

	data: string;
	constructor(public http:Http, private router: Router) {
		//check authorization
		if (localStorage.getItem("user_key")!=null){
			this.router.navigate(['/all_tasks']); 
		}
		this.message_user="";
	}
	registration(){
		let body = JSON.stringify({"option":"registration", "login": this.login, "password": md5(this.password) });
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: "post" });
		this.http.post("http://todo.webcreator34.ru", body, options).subscribe(res => {
				this.data = res.json();
				this.message_user=this.data["answer"];
				//if success registration
				if (this.data["success"]==1){
					//redirect to enter form
					setTimeout (() => {
						this.router.navigate(['/enter']); 
					}, 1000);
				}
			}
		);
	}
	private handleError (error: Response) {
        console.error(error);
    }
}