import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ExitComponent } from "./exit/exit.component";
import { Location } from '@angular/common'; 

@Component({
	selector: 'app-root',
	templateUrl: "app.component.html"
})

export class AppComponent {

	exit: boolean=false;
	exit_component: ExitComponent = new ExitComponent();
	constructor (private router: Router, private location: Location) {
		this.show_exit();
		//this.localStorageListener();
		setInterval(() => {
			this.show_exit();
		}, 500);
	}

	location_back(){
		this.location.back();
	}

	exit_us(){
		this.exit_component.exit_us();
		this.router.navigate(['/enter']); 
	}

	/*//set listener localStorage
	localStorageListener(){
		let originalSetItem = localStorage.setItem;
		let originalRemoveItem = localStorage.removeItem;
		localStorage.setItem = function() {
			let event = new Event('LocalStorageChange');
			originalSetItem.apply(this, arguments);
			document.dispatchEvent(event);
		}
		localStorage.removeItem = function() {
			let event = new Event('LocalStorageChange');
			originalRemoveItem.apply(this, arguments);
			document.dispatchEvent(event);
		}
		let ListenerStorage = function(e) {
			if (localStorage.getItem("user_key")!=null){
				this.exit=true;
			} else {
				this.exit=false;
			}
			alert(this.exit);
		};
        document.addEventListener("LocalStorageChange", ListenerStorage, false);
	}*/


	show_exit(){
		if (localStorage.getItem("user_key")!=null){
			this.exit=true;
		} else {
			this.exit=false;
		}
	}
}