import { Component } from "@angular/core";
import { Injectable } from "@angular/core";

@Injectable()
export class Section {
	id: Number=-1;
	header: String="";
	task: String="";
	type_task: String="";
	date: Date=new Date("");
	location: String="";
	time_start: Date=new Date("00:00");
	time_end: any="";
	constructor(input){
		this.id=input["id"];
		this.header=input["header"];
		this.task=input["task"];
		this.type_task=input["type_task"];
		this.date=new Date(input["date"]);
		this.location=input["location"];
		this.time_start=input["time_start"];
		if (input["time_end"]=="0000-00-00" || input["time_end"]=="" || typeof(input["time_end"])=="undefined"){
			this.time_end="";
		} else {
			this.time_end=new Date(input["time_end"]);
		}
	}
}
