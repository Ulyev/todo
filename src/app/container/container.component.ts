import { Component, OnInit} from "@angular/core";
import { Optional,Injectable } from "@angular/core";
import { Section } from "./section.component"
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";

@Injectable()
export class ContainerComponent{
	private all_tasks_arr: Section[] = [];

	is_loaded=false;//uploaded flag
	constructor(private http:Http){

	}

	/*
	* add value
	*/
	addValue(value){
		let body = JSON.stringify({
			"option":"create_task",
			"user_key":localStorage.getItem("user_key"),
			"type":value.type_task,
			"title":value.header,
			"place":value.location,
			"description":value.task,
			"date":value.date,
			"time":value.time_start
		});
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: "post" });
		let id;
		this.http.post("http://todo.webcreator34.ru", body, options).subscribe(res => {
			this.all_tasks_arr[this.all_tasks_arr.length]=new Section({
				"id":res.text(),
				"header":value.header,
				"task":value.task,
				"type_task":value.type_task,
				"date":value.date,
				"location":value.location,
				"time_start":value.time_start
			}); 
			this.all_tasks_arr=this.sortByDate(this.all_tasks_arr);
		});
	}

	getIndexById(id){
		for (let i=0; i<this.all_tasks_arr.length; i++){
			if (this.all_tasks_arr[i].id==id){
				return i;
			}
		}
	}

	/*
	* get value by index
	*/
	getValue(id){
		let index=this.getIndexById(id);
		if (index>=this.all_tasks_arr.length){
			return null;
		}
		return this.all_tasks_arr[index];
	}

	/*
	* del value by index
	*/
	delValue(id){
		let index=this.getIndexById(id);
		let body = JSON.stringify({"option":"delete_tasks", "id":this.all_tasks_arr[index].id, "user_key":localStorage.getItem("user_key")});
		delete this.all_tasks_arr[index];
		this.all_tasks_arr.splice(index, 1);
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: "post" });
		this.http.post("http://todo.webcreator34.ru", body, options).subscribe();
	}

	/*
	* change value by index
	*/
	changeValue(value, id){
		let index=this.getIndexById(id);
		let body = JSON.stringify({
			"option":"change_task",
			"id":value.id,
			"user_key":localStorage.getItem("user_key"),
			"type":value.type_task,
			"title":value.header,
			"place":value.location,
			"description":value.task,
			"date":value.date,
			"time":value.time_start
		});
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: "post" });
		this.http.post("http://todo.webcreator34.ru", body, options).subscribe(res => {
	
		});
		this.all_tasks_arr[index]=new Section(value);
	}

	/*
	* return all container
	*/
	getContainer(){
		return this.all_tasks_arr;
	}

	/*
	* delete all task in this container
	*/
	setClear(){
		this.all_tasks_arr.splice(0, this.all_tasks_arr.length);//delete elements
	}

	/*
	* get tasks from server for this user
	*/
	updateContainer(){
		let body = JSON.stringify({"option":"get_tasks", "user_key":localStorage.getItem("user_key")});
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers, method: "post" });
        this.http.post("http://todo.webcreator34.ru", body, options).subscribe(res => {
            if (res.text()!="NoTasks"){
                let table_tasks=res.json();//table tasks user
                for (let index in table_tasks){
                   this.all_tasks_arr[this.all_tasks_arr.length]=new Section(table_tasks[index]);
                }
            }
            this.is_loaded=true;//set upload value
        });
	}

	/*
	* return count in array
	*/
	getCount(){
		return this.all_tasks_arr.length;
	}

	/*
	* check is uploaded container array
	*/
	isUpload(){
		return this.is_loaded;
	}

	/*
	* mark current task as done
	*/
	made(id, date){
		let index=this.getIndexById(id);
		let body = JSON.stringify({"option":"set_made", "user_key":localStorage.getItem("user_key"), "id_task":this.all_tasks_arr[index].id, "time_end":date});
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers, method: "post" });
        this.http.post("http://todo.webcreator34.ru", body, options).subscribe();

        this.all_tasks_arr[index].time_end=date;
	}

	/*
	* sort container by date
	*/
	sortByDate(container){
		for (let i=0; i<container.length-1; i++){
			for (let k=0; k<container.length-1; k++){
				if (container[k].date<container[k+1].date){
					let temp=container[k];
					container[k]=container[k+1];
					container[k+1]=temp;
				}
			}
		}
		return container;
	}

	/*Filters block*/
	filter(filter){
		return this.filter_made(filter.made, this.filter_date(filter.date, this.filter_type(filter.type, this.all_tasks_arr)));
	}

	/*
	* filter by type
	*/
	filter_type(value, container){
		if (value==-1){
			return container;
		} else {
			let return_container: Section[] = [];
			for (let i=0; i<container.length; i++){
				if (container[i].type_task==value){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		}
	}

	/*
	* filter by date
	*/
	filter_date(value, container){
		let return_container: Section[] = [];	
		let now_date: Date=new Date();	
		let weeks_now: Number=this.getWeek(now_date);

		if (value==-1){
			return container;
		} else if (value==0){//filter by today
			for (let i=0; i<container.length; i++){
				if (container[i].date.getFullYear()==now_date.getFullYear() && container[i].date.getMonth()==now_date.getMonth() && container[i].date.getDate()==now_date.getDate()){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		} else if (value==1){//filter by this week
			for (let i=0; i<container.length; i++){
				if (this.getWeek(container[i].date)==weeks_now){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		} else {//filter by this month
			for (let i=0; i<container.length; i++){
				if (container[i].date.getMonth()==now_date.getMonth()){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		}
	}

	filter_made(value, container){
		let return_container: Section[] = [];	
		if (value==-1){
			return container;
		} else if (value==0){//filter by did it
			for (let i=0; i<container.length; i++){
				if (container[i].time_end!=""){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		} else if (value==1){//filter by should did it
			for (let i=0; i<container.length; i++){
				if (container[i].time_end==""){
					return_container[return_container.length]=container[i];
				}
			}
			return return_container;
		}
	}


	/*
	* Get numbers of week
	*/
	getWeek(currentDateTime){
		let target: any  = new Date(currentDateTime.valueOf());
		let dayNr: any   = (currentDateTime.getDay() + 6) % 7;
		target.setDate(target.getDate() - dayNr + 3);
		let firstThursday: any = target.valueOf();
		target.setMonth(0, 1);
		if (target.getDay() != 4) {
			target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
		}
		let result: Number=Math.ceil((firstThursday - target) / 604800000);
 		return result;
	}
}
