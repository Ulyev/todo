import { Component } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';

export class ExitComponent {
	constructor(){

	}

	public exit_us(){
		window["W_Container"].setClear();//clear all array
		window["exit"]=false;//hide window exit
		localStorage.removeItem("user_key");//remove session session
	}
}