import { Component } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: "create_task",
	templateUrl: "createtask.component.html",
	styleUrls: ["createtask.component.css"]
})

export class CreateTaskComponent {
	is_auth=localStorage.getItem("user_key")!=null;
	/*tabs*/
	tab: Number=0;

	/*form*/
	title: String="";
	place: String="";
	description: String="";
	date: String="";
	time: String="";

	constructor(public http:Http, private router: Router){
		if (localStorage.getItem("user_key")==null){
			this.router.navigate(['/enter']); 
		}
	}

	create_tabs(type){
		this.tab=type;
	}

	add_task(form){
		/*clear unused variable*/
		if (this.tab==0){
			this.place="";
		} else if (this.tab==2){
			this.place="";
			this.description="";
		}
		window["W_Container"].addValue({
			"header":this.title,
			"task":this.description,
			"type_task":this.tab,
			"date":this.date,
			"location":this.place,
			"time_start":this.time
		});
		form.reset();
		this.router.navigate(['/all_tasks']);
	}
}