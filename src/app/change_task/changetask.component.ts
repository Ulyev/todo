import { Component, OnInit } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
	moduleId: module.id,
	selector: "change_task",
	templateUrl: "changetask.component.html",
	styleUrls: ["changetask.component.css"]
})

export class ChangeTaskComponent {
	index: number;
	id: number;
	is_auth=localStorage.getItem("user_key")!=null;

	is_update:Boolean=false;//has the response from the server?

	/*tabs*/
	tab: Number=0;

	/*form*/
	title: String="";
	place: String="";
	description: String="";
	date: String="";
	time: String="";
	constructor(private router: Router, private route: ActivatedRoute){
		if (localStorage.getItem("user_key")==null){
			this.router.navigate(['/enter']); 
		}

		this.route.params.switchMap((params: Params) => this.index=params['index']).subscribe();
		
		//If the data from server not received
		if (!window["W_Container"].isUpload()){
			this.wait_answer();//Wait answer from server
		} else {
			this.set_value();
			this.is_update=true;//set update flag
		}
	}

	/*
	* Wait answer from server width tasks user
	*/
	wait_answer(){
		let interval=setInterval(() => {
			if (window["W_Container"].isUpload()){//check if data update
				this.set_value();
				this.is_update=true;//set update flag
				clearInterval(interval);// clear check is update
			}
		}, 500);
	}

	/*
	* Set value in DOM
	*/
	set_value(){
		let value=window["W_Container"].getValue(this.index);
		if (value==null){//if error index element
			this.router.navigate(['/all_tasks']); //redirect
		} else {
			this.title=value.header;
			this.description=value.task;
			this.tab=value.type_task;
			this.date=value.date.getFullYear()+"-0"+(value.date.getMonth()+1)+"-"+value.date.getDate();
			this.place=value.location;
			this.time=value.time_start;
			this.id=value.id;
		}
	}

	create_tabs(type){
		this.tab=type;
	}

	change(form){
		/*clear unused variable*/
		if (this.tab==0){
			this.place="";
		} else if (this.tab==2){
			this.place="";
			this.description="";
		}

		window["W_Container"].changeValue({
			"id":this.id,
			"header":this.title,
			"task":this.description,
			"type_task":this.tab,
			"date":this.date,
			"location":this.place,
			"time_start":this.time
		}, this.index);
		form.reset();
		this.router.navigate(['/all_tasks']); 
	}
}