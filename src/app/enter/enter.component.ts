import { Component } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { md5 } from '../md5/md5.component';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: "enter",
	templateUrl: "enter.component.html",
	styleUrls: ["enter.component.css"]
})

@Injectable()
export class EnterComponent {
	is_auth=localStorage.getItem("user_key")!=null;
	
	login: string; //login user
	password: string; //password user
	message_user: string; //message for user

	data: string;
	
	constructor(public http:Http, private router: Router) {
		//check authorization
		if (localStorage.getItem("user_key")!=null){
			this.router.navigate(['/all_tasks']); 
		}
		this.message_user="";
	}

	enter(form){
		let body = JSON.stringify({"option":"enter", "login": this.login, "password": md5(this.password) });
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: "post" });
		this.http.post("http://todo.webcreator34.ru", body, options).subscribe(res => {
			this.data = res.json();
			this.message_user=this.data["answer"];
			//redirect to Enter Form
			if (this.data["success"]==1){
				form.reset();
				localStorage.setItem("user_key", this.data["user_key"]);//set user id in session
				window["W_Container"].updateContainer();//get tasks from server
				setTimeout (() => {
					this.router.navigate(['/all_tasks']); 
				}, 2000);
			}
			this.password="";
		});
	}
}
