import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http, Response } from "@angular/http";
import { RouterModule, Routes } from "@angular/router"
import { Headers, RequestOptions } from "@angular/http";


import { AppComponent } from './app.component';

import { EnterComponent } from "./enter/enter.component";
import { RegistrationComponent } from "./registration/registration.component";
import { AllTasksComponent } from "./all_tasks/all_tasks.component";
import { CreateTaskComponent } from "./create_task/createtask.component";
import { ChangeTaskComponent } from "./change_task/changetask.component";

import { ContainerComponent} from "./container/container.component";


@NgModule({
    declarations: [
        AppComponent,
        EnterComponent,
        RegistrationComponent,
        AllTasksComponent,
        CreateTaskComponent,
        ChangeTaskComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot([
            { path: "enter", component: EnterComponent },
            { path: "registration", component: RegistrationComponent },
            { path: "all_tasks", component: AllTasksComponent },
            { path: "create_task", component: CreateTaskComponent },
            { path: "change_task/:index", component: ChangeTaskComponent },
            { path: "**", redirectTo: "enter", pathMatch: "full"}
        ])
    ],
    providers: [],
    exports: [ RouterModule ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
    constructor(private http:Http){
        window["W_Container"] =  new ContainerComponent(http);
        if (localStorage.getItem("user_key")!=null){
            window["W_Container"].updateContainer();
        }
    }
}
