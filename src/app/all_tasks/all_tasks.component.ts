import { 
	Component, 
	trigger,
    state,
    style,
    transition,
    animate,
    keyframes
} from "@angular/core";

import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/Rx";

import { ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subject }    from 'rxjs/Subject';

import { Section } from "../container/section.component"

@Component({
	moduleId: module.id,
	selector: "all_tasks",
	templateUrl: "all_tasks.component.html",
	styleUrls: ["all_tasks.component.css"]
})

@Injectable()
export class AllTasksComponent {
	public all_tasks_arr: Section[] = [];
	is_auth=localStorage.getItem("user_key")!=null;
	overflow_height: String=String(window.innerHeight-115-25-100-60)+"px";

	is_update:Boolean=false;//has the response from the server?
	
	modal_window: String="";//content modal window
	title_modal_window: String="";//title in modal window


	/*filter_block*/
	is_filter: boolean=false;
	type_filter: Number=-1;
	date_filter: Number=-1;
	made_filter: Number=-1;

	constructor(public http:Http, private router: Router) {
		if (localStorage.getItem("user_key")==null){
			this.router.navigate(['/enter']); 
		} else {
			//If the data from server not received
			if (!window["W_Container"].isUpload()){
				this.wait_answer();//Wait answer from server
			} else {
				this.all_tasks_arr=window["W_Container"].sortByDate(window["W_Container"].getContainer());//update container
				this.is_update=true;//set update flag
			}
		}
	}

	/*
	* Wait answer from server width tasks user
	*/
	wait_answer(){
		let interval=setInterval(() => {
			if (window["W_Container"].isUpload()){//check if data update
				this.all_tasks_arr=window["W_Container"].sortByDate(window["W_Container"].getContainer());//update container
				clearInterval(interval);// clear check is update
				this.is_update=true;//set update flag
			}
		}, 500);
	}

	/*align window*/
	/*smooth(){
		let margin_left=String(document.getElementById("my_block").offsetWidth/2*-1)+"px";
		document.getElementById("my_block").style.marginLeft = margin_left;
		let height=String(window.innerHeight-115-25)+"px";
		document.getElementById("my_block").style.height = height;
	}*/

	/*
	* delete task
	*/
	delete(index){
		if (localStorage.getItem("user_key")==null){
			this.router.navigate(['/enter']); 
		} else {
			window["W_Container"].delValue(index);
			delete this.all_tasks_arr[index];
			this.all_tasks_arr.splice(index, 1);
		}

		//if filter is enabled
		if (window["W_Container"].getCount()==0){//If removed when the filter is enabled
			this.is_filter=false;
		}
	}

	/*
	* get change component
	*/
	change(id){
		this.router.navigate(['/change_task', id]); 
	}

	/*
	* conversion index tab in type sctring
	*/
	change_type(tab){
		if (tab==0){
			return "Task";
		} else if (tab==1){
			return "Meeting";
		} else {
			return "Reminder";
		}
	}

	/*
	* exit modal window
	*/
	exit_modal(){
		this.modal_window="";
	}

	/*
	* show modal window with task description
	*/
	show_task_description(index){
		this.modal_window=window["W_Container"].getValue(index).task.replace(/\r/g, "<br/>");
		this.title_modal_window="My Task";
	}

	/*
	* conversion month index in string month name
	*/
	getMonthName(number_month) {
		var month = ['Jan','Feb','Mar','Apr','May','Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'];
		return month[number_month];
	}

	/*
	* mark the task as mentioned
	*/
	made(id, index){//the index can be if you use filters, not a real index
		let date=new Date();
		this.all_tasks_arr[index].time_end=date;
		window["W_Container"].made(id, date);
	}

	/*
	* show all filters in modal window
	*/
	setFilter(filter, value){
		if (filter==0){//if filter by type
			this.type_filter=value;
		} else if (filter==1){
			this.date_filter=value;
		} else {
			this.made_filter=value;
		}

		this.all_tasks_arr=window["W_Container"].filter({
			"type": this.type_filter,
			"date": this.date_filter,
			"made": this.made_filter
		});

		/*if is all filters close*/
		if (this.type_filter==-1 && this.date_filter==-1 && this.made_filter==-1){
			this.is_filter=false;
		} else {
			this.is_filter=true;
		}
	}
}